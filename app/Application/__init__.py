from flask import Flask

print ('Creating app...')
app = Flask(__name__)
#app.config['PROPAGATE_EXCEPTIONS'] = True

@app.route('/')
def hello():
    print ('Processing request...')
    return "Hello World! (Application)"

